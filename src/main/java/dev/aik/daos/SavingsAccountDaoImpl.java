package dev.aik.daos;

import dev.aik.models.SavingsAccount;
import dev.aik.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SavingsAccountDaoImpl implements SavingsAccountDao{

    @Override
    public String generateSavingsAccountNumber(Long phoneNumber) {
            int rand1 = (int) (Math.random() * Math.pow(10,3));
            String phone = String.valueOf(phoneNumber);
            String phonePart = phone.substring(6,10);
            int rand2 = (int) (Math.random() * Math.pow(10,3));
            String generalNumberAcc = rand1 + phonePart + rand2;
            int checkIdentifier = 0;
            String savingsAccountNumber = checkIdentifier + generalNumberAcc;
            return savingsAccountNumber;
    }

    public String savingsAccountFound(int userId){
        String sql = "select sai.savings_number from savings_account_info sai where sai.savingsuser_id = ?";
        String savingsNumber = "";
        try(Connection connection = ConnectionUtil.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql)){
            callableStatement.setInt(1,userId);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            if(resultSet.next()){
                savingsNumber = resultSet.getString("savings_number");
                System.out.println(" Savings Account number is: "+savingsNumber);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return savingsNumber;
    }

    @Override
    public boolean createSavingsAccount(SavingsAccount newAccount) {
       String sql = "insert into savings_account_info (id, savings_number, savings_balance, savings_deposit, savings_withdraw, transaction_date, savingsuser_id) values (default,?, 0, 0, 0, ?, ?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1, newAccount.getSavingsAccountNumber());
            ps.setString(2, newAccount.getDateTime());
            ps.setInt(3,newAccount.getSavingUserId());
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public double viewSavingBalance(int userId) {
        String sql = "select sai.savings_balance, sai.transaction_date from savings_account_info sai where savingsuser_id = ? order by sai.id desc";
        double balance= 0.0;
        try(Connection connection = ConnectionUtil.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql)){
            callableStatement.setInt(1,userId);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            if(resultSet.next()){
               balance  = resultSet.getDouble("savings_balance");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return balance;
    }

    @Override
    public boolean depositSavings(SavingsAccount newDeposit) {
        String sql = "insert into savings_account_info (savings_number, savings_balance, savings_deposit, savings_withdraw, transaction_date, savingsuser_id) values(?, ?, ?, ?, ?, ?)";
        try(Connection connection = ConnectionUtil.getConnection();
        PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setString(1,newDeposit.getSavingsAccountNumber());
            ps.setDouble(2,newDeposit.getSavingsBalance());
            ps.setDouble(3,newDeposit.getSavingsDepositAmount());
            ps.setDouble(4,newDeposit.getSavingsWithdrawAmount());
            ps.setObject(5,newDeposit.getDateTime());
            ps.setInt(6,newDeposit.getSavingUserId());
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean withdrawSavings(SavingsAccount newWithdraw) {
        String sql = "insert into savings_account_info (savings_number, savings_balance, savings_deposit, savings_withdraw, transaction_date, savingsuser_id) values(?, ?, ?, ?, ?, ?)";
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setString(1,newWithdraw.getSavingsAccountNumber());
            ps.setDouble(2,newWithdraw.getSavingsBalance());
            ps.setDouble(3,newWithdraw.getSavingsDepositAmount());
            ps.setDouble(4,newWithdraw.getSavingsWithdrawAmount());
            ps.setObject(5,newWithdraw.getDateTime());
            ps.setInt(6,newWithdraw.getSavingUserId());
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean transferSavings() {
        return false;
    }

    @Override
    public List<SavingsAccount> transactionHistorySavings(int userId) {
        String sql = "select savings_balance, savings_deposit, savings_withdraw, transaction_date from savings_account_info where savingsuser_id = ? order by transaction_date";
        try (Connection connection = ConnectionUtil.getConnection();
             CallableStatement callableStatement = connection.prepareCall(sql)){
            callableStatement.setInt(1,userId);
            callableStatement.execute();
            ResultSet rs = callableStatement.getResultSet();
            List<SavingsAccount> transactions = new ArrayList<>();
            while(rs.next()){
                SavingsAccount sa = new SavingsAccount();
                double balance = rs.getDouble("savings_balance");
                double deposit = rs.getDouble("savings_deposit");
                double withdraw = rs.getDouble("savings_withdraw");
                String date = rs.getString("transaction_date");
                sa.setSavingsBalance(balance);
                sa.setSavingsDepositAmount(deposit);
                sa.setSavingsWithdrawAmount(withdraw);
                sa.setDate(date);
                transactions.add(sa);
            }
            return transactions;
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
