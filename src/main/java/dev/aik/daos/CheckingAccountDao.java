package dev.aik.daos;

import dev.aik.models.CheckingAccount;
import dev.aik.models.SavingsAccount;

import java.util.List;

public interface CheckingAccountDao {
    public String generateCheckingAccountNumber(Long phoneNumber);
    public boolean createCheckingAccount(CheckingAccount newAccount);
    public double viewCheckingBalance(int userId);
    public boolean depositChecking(CheckingAccount newDeposit);
    public boolean withdrawChecking(CheckingAccount newWithdraw);
    public boolean transferChecking();
    public List<CheckingAccount> transactionHistoryChecking(int userId);
    public String checkingAccountFound(int userId);
}
