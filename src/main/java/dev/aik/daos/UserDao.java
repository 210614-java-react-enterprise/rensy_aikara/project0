package dev.aik.daos;

import dev.aik.models.User;

import java.util.List;

public interface UserDao {


    //method to display all Users(name)
    //method to Create new User account
    //method to check whether an account already exits
    //method to get a particular User Account details


    public List<User> getAllUsers();
    public boolean createUserAccount(User newUser);
    public String existingAccount(String newUserEmail);
    public String checkPassword(String userEmail);
    public User accountDetails(String userEmail);
    //public boolean updateLoggedIn(int id);
}
