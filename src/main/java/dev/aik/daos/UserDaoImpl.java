package dev.aik.daos;

import dev.aik.models.User;
import dev.aik.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao{
    @Override
    public List<User> getAllUsers() {
        String sql = "select ut.email_id from user_table ut";

        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)){
            List<User> users = new ArrayList<>();
            while(rs.next()){
                User us = new User();
                String newEmail = rs.getString("email_id");
                us.setEmailId(newEmail);
                users.add(us);
            }
            return users;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public String existingAccount(String newUserEmail) {
        String sql = "select ut.account_name from user_table ut where ut.email_id = ?";
        String existingName = "";
        try (Connection connection = ConnectionUtil.getConnection();
             CallableStatement callableStatement = connection.prepareCall(sql)) {
            callableStatement.setString(1, newUserEmail);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            if(resultSet.next()){
                existingName = resultSet.getString("account_name");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return existingName;
    }

    public String checkPassword(String userEmail){
        String sql = "select ut.user_password from user_table ut where ut.email_id = ?";
        String userPassword = "";
        try (Connection connection = ConnectionUtil.getConnection();
             CallableStatement callableStatement = connection.prepareCall(sql)) {
            callableStatement.setString(1, userEmail);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            if(resultSet.next()) {
                userPassword = resultSet.getString("user_password");
               // System.out.println("Password saved: " +userPassword);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return userPassword;
    }

    public User accountDetails(String userEmail){
        String sql = "select ut.id, ut.account_name, ut.phone_number, ut.loggedin from user_table ut where ut.email_id = ?";
        try(Connection connection = ConnectionUtil.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql)) {
            callableStatement.setString(1, userEmail);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            User userDetails = new User();
            if(resultSet.next()) {
                userDetails.setId(resultSet.getInt("id"));
                userDetails.setName(resultSet.getString("account_name"));
                userDetails.setPhoneNumber(resultSet.getLong("phone_number"));
                userDetails.setLoggedIn(true);
            }
            return userDetails;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
/*
    public boolean updateLoggedIn(int id){
        String sql = "update user_table set loggedin = ? where id = ?";
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setBoolean(1,true);
            ps.setInt(2, id);
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

 */
        @Override
    public boolean createUserAccount(User newUser) {
        String sql = "insert into user_table values(default, ?, ?, ?, ?)";
        try( Connection connection = ConnectionUtil.getConnection();
        PreparedStatement ps = connection.prepareStatement(sql);) {

                   //ps.setInt(1, newUser.getId());
                   ps.setString(1, newUser.getName());
                   ps.setString(2, newUser.getEmailId());
                   ps.setLong(3, newUser.getPhoneNumber());
                   ps.setString(4,newUser.getPassword());
                   int success = ps.executeUpdate();
                   if(success>0){
                       return true;
                   }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
