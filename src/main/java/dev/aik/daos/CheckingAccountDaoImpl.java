package dev.aik.daos;

import dev.aik.models.CheckingAccount;
import dev.aik.models.SavingsAccount;
import dev.aik.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CheckingAccountDaoImpl implements CheckingAccountDao{

    @Override
    public String generateCheckingAccountNumber(Long phoneNumber) {
        int rand1 = (int) (Math.random() * Math.pow(10,3));
        String phone = String.valueOf(phoneNumber);
        String phonePart = phone.substring(6,10);
        int rand2 = (int) (Math.random() * Math.pow(10,3));
        String generalNumberAcc = rand1 + phonePart + rand2;
        int checkIdentifier = 1;
        String checkingAccountNumber = checkIdentifier + generalNumberAcc;
        return checkingAccountNumber;
    }

    @Override
    public boolean createCheckingAccount(CheckingAccount newAccount) {
        String sql = "insert into checking_account_info (id, checking_number, checking_balance, checking_deposit, checking_withdraw, transaction_date, checkinguser_id) values (default,?, 0, 0, 0, ?, ?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1, newAccount.getCheckingAccountNumber());
            ps.setString(2, newAccount.getDateTime());
            ps.setInt(3,newAccount.getCheckingUserId());
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public double viewCheckingBalance(int userId) {
        String sql = "select cai.checking_balance, cai.transaction_date from checking_account_info cai where checkinguser_id = ? order by cai.id desc";
        double balance= 0.0;
        try(Connection connection = ConnectionUtil.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql)){
            callableStatement.setInt(1,userId);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            if(resultSet.next()){
                balance  = resultSet.getDouble("checking_balance");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return balance;
    }

    @Override
    public boolean depositChecking(CheckingAccount newDeposit) {
        String sql = "insert into checking_account_info (checking_number, checking_balance, checking_deposit, checking_withdraw, transaction_date, checkinguser_id) values(?, ?, ?, ?, ?, ?)";
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setString(1,newDeposit.getCheckingAccountNumber());
            ps.setDouble(2,newDeposit.getCheckingBalance());
            ps.setDouble(3,newDeposit.getCheckingDepositAmount());
            ps.setDouble(4,newDeposit.getCheckingWithdrawAmount());
            ps.setObject(5,newDeposit.getDateTime());
            ps.setInt(6,newDeposit.getCheckingUserId());
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean withdrawChecking(CheckingAccount newWithdraw) {
        String sql = "insert into checking_account_info (checking_number, checking_balance, checking_deposit, checking_withdraw, transaction_date, checkinguser_id) values(?, ?, ?, ?, ?, ?)";
        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setString(1,newWithdraw.getCheckingAccountNumber());
            ps.setDouble(2,newWithdraw.getCheckingBalance());
            ps.setDouble(3,newWithdraw.getCheckingDepositAmount());
            ps.setDouble(4,newWithdraw.getCheckingWithdrawAmount());
            ps.setObject(5,newWithdraw.getDateTime());
            ps.setInt(6,newWithdraw.getCheckingUserId());
            int success = ps.executeUpdate();
            if(success>0){
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean transferChecking() {
        return false;
    }

    @Override
    public List<CheckingAccount> transactionHistoryChecking(int userId) {
        String sql = "select checking_balance, checking_deposit, checking_withdraw, transaction_date from checking_account_info where checkinguser_id = ? order by transaction_date";
        try (Connection connection = ConnectionUtil.getConnection();
             CallableStatement callableStatement = connection.prepareCall(sql)){
            callableStatement.setInt(1,userId);
            callableStatement.execute();
            ResultSet rs = callableStatement.getResultSet();
            List<CheckingAccount> transactions = new ArrayList<>();
            while(rs.next()){
                CheckingAccount sa = new CheckingAccount();
                double balance = rs.getDouble("checking_balance");
                double deposit = rs.getDouble("checking_deposit");
                double withdraw = rs.getDouble("checking_withdraw");
                String date = rs.getString("transaction_date");
                sa.setCheckingBalance(balance);
                sa.setCheckingDepositAmount(deposit);
                sa.setCheckingWithdrawAmount(withdraw);
                sa.setDateTime(date);
                transactions.add(sa);
            }
            return transactions;
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public String checkingAccountFound(int userId) {
        String sql = "select cai.checking_number from checking_account_info cai where cai.checkinguser_id = ?";
        String checkingNumber = "";
        try(Connection connection = ConnectionUtil.getConnection();
            CallableStatement callableStatement = connection.prepareCall(sql)){
            callableStatement.setInt(1,userId);
            callableStatement.execute();
            ResultSet resultSet = callableStatement.getResultSet();
            if(resultSet.next()){
                checkingNumber = resultSet.getString("checking_number");
                System.out.println(" Checking Account number is: "+checkingNumber);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return checkingNumber;
    }

}
