package dev.aik.models;

import java.util.Objects;

public class User {
    private int id;
    private String name;
    private String password;
    private Long phoneNumber;
    private String emailId;
    private boolean loggedIn;

    public User(){
        super();
    }

    public User(String name, String emailId, Long phoneNumber, String password){
        super();
       // this.id = id;
        this.name = name;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.emailId = emailId;
    }


    //getters and setters
    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(name, user.name) && Objects.equals(password, user.password) && Objects.equals(phoneNumber, user.phoneNumber) && Objects.equals(emailId, user.emailId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, password, phoneNumber, emailId);
    }

    //toString method
    @Override
    public String toString() {
        return "User{" +
               // "id=" + id +
               // ", name='" + name + '\'' +
                //", password='" + password + '\'' +
               // ", phoneNumber=" + phoneNumber +
               ", emailId='" + emailId + '\'' +
                '}';
    }
}
