package dev.aik.models;


import java.util.Objects;

public class SavingsAccount {
    private int id;
    private String savingsAccountNumber;
    private double savingsBalance;
    private double savingsDepositAmount;
    private double savingsWithdrawAmount;
    private String dateTime;
    private int savingUserId;

    public SavingsAccount(){
        super();
    }

    public SavingsAccount(String savingsAccountNumber, Double savingsBalance, Double savingsDepositAmount, Double savingsWithdrawAmount,String dateTime, int savingUserId){
        super();
        this.savingsAccountNumber = savingsAccountNumber;
        this.savingsBalance = savingsBalance;
        this.savingsDepositAmount = savingsDepositAmount;
        this.savingsWithdrawAmount = savingsWithdrawAmount;
        this.dateTime = dateTime;
        this.savingUserId = savingUserId;
    }

    public int getSavingUserId() {
        return savingUserId;
    }

    public void setSavingUserId(int savingUserId) {
        this.savingUserId = savingUserId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSavingsAccountNumber() {
        return savingsAccountNumber;
    }

    public void setSavingsAccountNumber(String savingsAccountNumber) {
        this.savingsAccountNumber = savingsAccountNumber;
    }

    public double getSavingsBalance() {
        return savingsBalance;
    }

    public void setSavingsBalance(double savingsBalance) {
        this.savingsBalance = savingsBalance;
    }

    public double getSavingsDepositAmount() {
        return savingsDepositAmount;
    }

    public void setSavingsDepositAmount(double savingsDepositAmount) {
        this.savingsDepositAmount = savingsDepositAmount;
    }

    public double getSavingsWithdrawAmount() {
        return savingsWithdrawAmount;
    }

    public void setSavingsWithdrawAmount(double savingsWithdrawAmount) {
        this.savingsWithdrawAmount = savingsWithdrawAmount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDate(String dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SavingsAccount that = (SavingsAccount) o;
        return Double.compare(that.savingsBalance, savingsBalance) == 0 && Double.compare(that.savingsDepositAmount, savingsDepositAmount) == 0 && Double.compare(that.savingsWithdrawAmount, savingsWithdrawAmount) == 0 && Objects.equals(savingsAccountNumber, that.savingsAccountNumber) && Objects.equals(dateTime, that.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(savingsAccountNumber, savingsBalance, savingsDepositAmount, savingsWithdrawAmount, dateTime);
    }

    @Override
    public String toString() {
        return "SavingsAccount{" +
              //  "savingsAccountNumber='" + savingsAccountNumber + '\'' +
                ", savingsBalance=" + savingsBalance +
                ", savingsDepositAmount=" + savingsDepositAmount +
                ", savingsWithdrawAmount=" + savingsWithdrawAmount +
                ", date=" + dateTime +
                '}';
    }

}
