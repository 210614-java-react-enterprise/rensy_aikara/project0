package dev.aik.models;


import java.util.Objects;

public class CheckingAccount {
    private int id;
    private String checkingAccountNumber;
    private double checkingBalance;
    private double checkingDepositAmount;
    private double checkingWithdrawAmount;
    private String dateTime;
    private int checkingUserId;

    public CheckingAccount(){
        super();
    }

    public CheckingAccount(String checkingAccountNumber, Double checkingBalance, Double checkingDepositAmount, Double checkingWithdrawAmount,String dateTime, int checkingUserId){
        super();
        this.checkingAccountNumber = checkingAccountNumber;
        this.checkingBalance = checkingBalance;
        this.checkingDepositAmount = checkingDepositAmount;
        this.checkingWithdrawAmount = checkingWithdrawAmount;
        this.dateTime = dateTime;
        this.checkingUserId = checkingUserId;
    }

    public int getCheckingUserId() {
        return checkingUserId;
    }

    public void setCheckingUserId(int checkingUserId) {
        this.checkingUserId = checkingUserId;
    }

    public String getCheckingAccountNumber() {
        return checkingAccountNumber;
    }

    public void setCheckingAccountNumber(String checkingAccountNumber) {
        this.checkingAccountNumber = checkingAccountNumber;
    }

    public double getCheckingBalance() {
        return checkingBalance;
    }

    public void setCheckingBalance(double balance) {
        this.checkingBalance = balance;
    }

    public double getCheckingDepositAmount() {
        return checkingDepositAmount;
    }

    public void setCheckingDepositAmount(double depositAmount) {
        this.checkingDepositAmount = depositAmount;
    }

    public double getCheckingWithdrawAmount() {
        return checkingWithdrawAmount;
    }

    public void setCheckingWithdrawAmount(double withdrawAmount) {
        this.checkingWithdrawAmount = withdrawAmount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckingAccount that = (CheckingAccount) o;
        return Double.compare(that.checkingBalance, checkingBalance) == 0 && Double.compare(that.checkingDepositAmount, checkingDepositAmount) == 0 && Double.compare(that.checkingWithdrawAmount, checkingWithdrawAmount) == 0 && Objects.equals(checkingAccountNumber, that.checkingAccountNumber) && Objects.equals(dateTime, that.dateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(checkingAccountNumber, checkingBalance, checkingDepositAmount, checkingWithdrawAmount, dateTime);
    }

    @Override
    public String toString() {
        return "CheckingAccount{" +
              //  "checkingAccountNumber='" + checkingAccountNumber + '\'' +
                ", balance=" + checkingBalance +
                ", depositAmount=" + checkingDepositAmount +
                ", withdrawAmount=" + checkingWithdrawAmount +
                ", date=" + dateTime +
                '}';
    }
}
